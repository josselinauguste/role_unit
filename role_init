#!/usr/bin/env bash

#    (c) 2017-2016, n0vember <n0vember@half-9.net>
#
#    This file is part of role_unit.
#
#    role_unit is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    role_unit is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with role_unit.  If not, see <http://www.gnu.org/licenses/>.

# Functions
usage() {
  my_name=$(basename $0)
  [ $# -gt 0 ] && echo "$@" >&2
  echo "usage : ${my_name} [ -v|--verbose ] [ -h|--help ] ROLE_NAME ...
  creates a ROLE_NAME directory in the current directory, with a dummy role and unit tests embeded.
  if multiple role names are specified a role directory will be created for each.
  -v|--verbose : activates verbose mode
     -h|--help : displays this help" >&2
  exit 1
}

read_link() {
  local link=$1
  local path=$(dirname ${link})
  local file=$(basename ${link})
  cd ${path}
  if [ -L ${file} ] ; then
    file=$(readlink ${file})
    file=$(read_link ${file})
  else
    local dir=$(pwd)
    echo -n ${dir}/
  fi
  echo ${file}
}

create_an_ansible_role() {
  local role_name=$1
  # Create empty role
  mkdir ${verbose_flag} -p ${role_name}/{,tasks,meta}
  [ ! -f ${role_name}/tasks/main.yml ] && cp ${verbose_flag} ${role_unit_dir}/role_files/tasks ${role_name}/tasks/main.yml
  # Dependencies to make role usable by ansible-galaxy
  [ ! -f ${role_name}/meta/main.yml ] && cp ${verbose_flag} ${role_unit_dir}/role_files/meta ${role_name}/meta/main.yml
}

make_directory_testable() {
  local role_name=$1
  # Testing environment
  mkdir ${verbose_flag} -p ${role_name}/tests
  cp ${verbose_flag} ${role_unit_dir}/tools/bash_unit ${role_name}/tests
  cp ${verbose_flag} -r ${role_unit_dir}/lib ${role_name}/tests
  [ ! -f ${role_name}/tests/ansible.cfg ] && cp ${verbose_flag} ${role_unit_dir}/tools/ansible.cfg ${role_name}/tests
  [ ! -f ${role_name}/tests/tests_${role_name} ] && cp ${verbose_flag} ${role_unit_dir}/tools/tests ${role_name}/tests/tests_${role_name}
  [ ! -f ${role_name}/.gitlab-ci.yml ] \
    && cp ${verbose_flag} ${role_unit_dir}/tools/gitlab-ci.yml ${role_name}/.gitlab-ci.yml \
    && sed -i -e "s:ROLE_NAME:${role_name}:g" ${role_name}/.gitlab-ci.yml
}

explain() {
  cat <<EOF
$1 role created. Go in $1 directory and...
  Start testing now with the following command:
  RU_ENV_IMAGE=debian9 tests/bash_unit tests/tests_$1
  Look for help in tools/tests_tuto file and run it:
  RU_ENV_IMAGE=centos7 tests/bash_unit tools/tests_tuto
  Change the image you use to match your needs and code your role.

EOF
}

# Check parameters
getopt -o vh --long verbose,help -n 'option parsing' -- "$@" > /dev/null
[ $? != 0 ] && usage "Failed parsing options."

verbose_mode=0
verbose_flag=

while true; do
  case "$1" in
    -v | --verbose )
      verbose_mode=1
      verbose_flag="-v"
      shift
      ;;
    -h | --help )
      usage
      ;;
    -- ) shift; break ;;
    * ) break ;;
  esac
done

[ $# -lt 1 ] && usage "role name is mandatory"

# determine where we create dir and where script really is
role_dir=$(pwd)
role_unit_dir=$(dirname $(read_link $0))

while [ $# -ge 1 ]
do
  create_an_ansible_role $1
  make_directory_testable $1
  [ ${verbose_mode} -eq 1 ] && explain $1
  shift
done
